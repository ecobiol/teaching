Economic Principles in Biology
------------------------------

This repository stores only the files used for our [website](https://ecobiol.gitlab.io/teaching/).

If you want to submit your own teaching material, please use the [`teaching-material`](https://gitlab.com/ecobiol/teaching-material) repository.
